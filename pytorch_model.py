import torch
import torch.utils.model_zoo as model_zoo
import torchvision
import PIL
import numpy as np
import pickle
import random

# adapted from https://github.com/pytorch/vision/blob/master/torchvision/models/resnet.py

weights_folder = "/weights/"
PATH = weights_folder + "wscnet_new.pth"
model_url = 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth'

dataset_datafile = "ground_truth.pickle"
dataset_folder_loc = "/dataset/"

train_checkpoint_filename = "TRAIN_RESULT.pth"

# network

def conv3x3(in_planes, out_planes, stride = 1):
	return torch.nn.Conv2d(in_planes, out_planes, kernel_size = 3, stride = stride, padding = 1, bias = False)

def conv1x1(in_planes, out_planes, stride = 1):
	return torch.nn.Conv2d(in_planes, out_planes, kernel_size = 1, stride = stride, bias = False)

class s_list(list):

	def __init__(self, *args, **kwargs):
		super(s_list, self).__init__(*args, **kwargs)

	def size(self):
		return torch.Size([1])

class Bottleneck(torch.nn.Module):
	expansion = 4

	def __init__(self, in_planes, planes, stride = 1, downsample = None):
		super(Bottleneck, self).__init__()
		self.conv1 = conv1x1(in_planes, planes)
		self.bn1 = torch.nn.BatchNorm2d(planes)
		self.conv2 = conv3x3(planes, planes, stride)
		self.bn2 = torch.nn.BatchNorm2d(planes)
		self.conv3 = conv1x1(planes, planes * self.expansion)
		self.bn3 = torch.nn.BatchNorm2d(planes * self.expansion)
		self.relu = torch.nn.ReLU(inplace = True)
		self.downsample = downsample
		self.stride = stride

	def forward(self, x):
		identity = x

		out = self.conv1(x)
		out = self.bn1(out)
		out = self.relu(out)

		out = self.conv2(out)
		out = self.bn2(out)
		out = self.relu(out)

		out = self.conv3(out)
		out = self.bn3(out)

		if self.downsample is not None:
			identity = self.downsample(x)

		out += identity
		out = self.relu(out)

		return out

class MapGenerator(torch.nn.Module):

	def __init__(self, fm_num, num_classes):
		super(MapGenerator, self).__init__()
		self.fm_num = fm_num
		self.num_classes = num_classes

	def forward(self, x):
		fm, v = x
		n, c, w, h = fm.shape
		s_map = torch.zeros(n, h, w)
		# torch.zeros_like(s_map)
		# v_new = torch.empty(n, c)
		# for i in range(0, n):
		# 	for j in range(0, self.num_classes):
		# 		for k in range(0, self.fm_num):
		# 			v_new[i][j] += v[i][(j * self.fm_num) + k]
		for i in range(0, n):
			for j in range(0, self.num_classes):
				c_map = torch.zeros(w, h)
				# torch.zeros_like(c_map)
				for k in range(0, self.fm_num):
					c_map += fm[i, (j * self.fm_num) + k, :, :]
				t = torch.tensor([1.0 / self.fm_num])
				c_map = t * c_map
				# print(v[i][j])
				# print(c_map)
				s_map[i] += v[i][j] * c_map
				# print(s_map)
		return s_map

class PoolVector(torch.nn.Module):

	def __init__(self, fm_num, num_classes):
		super(PoolVector, self).__init__()
		self.fm_num = fm_num
		self.num_classes = num_classes

	def forward(self, x):
		v = x
		n, c, w, h = v.shape
		v_new = torch.zeros(n, self.num_classes, 1, 1)
		for i in range(0, n):
			for j in range(0, self.num_classes):
				for k in range(0, self.fm_num):
					v_new[i, j] += v[i][(j * self.fm_num) + k]
			t = torch.tensor([1.0 / self.fm_num])
			v_new = t * v_new
		return v_new

class HadamardProduct(torch.nn.Module):

	def __init__(self, fm_num, num_classes):
		super(HadamardProduct, self).__init__()

	def forward(self, x):
		fm, s_map = x
		n, c, h, w = fm.shape
		fm_s_map = torch.zeros(n, c, h, w)
		# torch.zeros_like(fm_s_map)
		for i in range(0, n):
			for j in range(0, c):
				fm_s_map[i, j, :, :] = fm[i, j, :, :] * s_map[i]
		fm_s_map = torch.cat((fm, fm_s_map), dim=1)
		return fm_s_map

class WSCNet(torch.nn.Module):

	def __init__(self, block, layers, fm_num = 4, num_classes = 8, zero_init_residual = False):
		super(WSCNet, self).__init__()
		self.inplanes = 64
		self.conv1 = torch.nn.Conv2d(3, 64, kernel_size = 7, stride = 2, padding = 3, bias = False)
		self.bn1 = torch.nn.BatchNorm2d(64)
		self.relu = torch.nn.ReLU(inplace = True)
		self.maxpool = torch.nn.MaxPool2d(kernel_size = 3, stride = 2, padding = 1)
		self.layer1 = self._make_layer(block, 64, layers[0])
		self.layer2 = self._make_layer(block, 128, layers[1], stride = 2)
		self.layer3 = self._make_layer(block, 256, layers[2], stride = 2)
		self.layer4 = self._make_layer(block, 512, layers[3], stride = 2)

		# wscnet branch

		# detection branch

		self.conv2 = torch.nn.Conv2d(512 * block.expansion, fm_num * num_classes, kernel_size = 1, stride = 1)
		# self.det_maxpool = torch.nn.MaxPool2d(kernel_size = 512, stride = 1, padding = 0)
		self.det_maxpool = torch.nn.AdaptiveMaxPool2d((1, 1))
		self.pooling_vector = PoolVector(fm_num, num_classes)
		self.softmax = torch.nn.Softmax(1)
		# self.cross_spatial = CrossSpatial(fm_num * num_classes, fm_num, num_classes)
		self.map_gen = MapGenerator(fm_num, num_classes)
		
		# classification branch

		self.hadamard = HadamardProduct(fm_num, num_classes)
		# merge f and u
		# self.class_avgpool = torch.nn.AvgPool2d(kernel_size = 512, stride = 1, padding = 0)
		self.class_avgpool = torch.nn.AdaptiveAvgPool2d((1, 1))
		self.fc = torch.nn.Linear(2 * 512 * block.expansion, num_classes, bias = False)
		self.relu2 = torch.nn.ReLU(inplace = True)
		self.softmax2 = torch.nn.Softmax(1)

		# end of wscnet

		num = 0

		for m in self.modules():
			num += 1
			if isinstance(m, torch.nn.Conv2d):
				torch.nn.init.kaiming_normal_(m.weight, mode = "fan_out", nonlinearity = "relu")
			elif isinstance(m, torch.nn.BatchNorm2d):
				torch.nn.init.constant_(m.weight, 1)
				torch.nn.init.constant_(m.bias, 0)

		if zero_init_residual:
			for m in self.modules():
				if isinstance(m, Bottleneck):
					torch.nn.init.constant_(m.bn3.weight, 0)

	def _make_layer(self, block, planes, blocks, stride = 1):
		downsample = None
		if stride != 1 or self.inplanes != planes * block.expansion:
			downsample = torch.nn.Sequential(conv1x1(self.inplanes, planes * block.expansion, stride), torch.nn.BatchNorm2d(planes * block.expansion))
		layers = []
		layers.append(block(self.inplanes, planes, stride, downsample))
		self.inplanes = planes * block.expansion
		for _ in range(1, blocks):
			layers.append(block(self.inplanes, planes))

		return torch.nn.Sequential(*layers)

	def forward(self, x):
		x = self.conv1(x)
		x = self.bn1(x)
		x = self.relu(x)
		x = self.maxpool(x)

		x = self.layer1(x)
		x = self.layer2(x)
		x = self.layer3(x)
		x = self.layer4(x)

		fm = self.conv2(x)
		# print(fm)
		v = self.det_maxpool(fm)
		v = self.pooling_vector(v)
		v = self.softmax(v)
		# print(v)
		gab = s_list((fm, v))
		# print(gab)
		s_map = self.map_gen(gab)
		# print(s_map)

		gab = s_list((x, s_map))
		f_u = self.hadamard(gab)
		x2 = self.class_avgpool(f_u)
		# print(x2)
		x2 = x2.view(x2.size(0), -1)
		x2 = self.fc(x2)
		# print("fc")
		# print(x2)
		# x2 = self.relu2(x2)
		# print("relu")
		# print(x2)
		res = self.softmax2(x2)
		# print("softmax")
		# print(res)

		return (res, v)

def custom_loss(y_pred, res, v):
	n, c = y_pred.shape
	l_dec = torch.zeros(1)
	# torch.zeros_like(l_dec)
	l_clx = torch.zeros(1)
	# torch.zeros_like(l_clx)
	for i in range(0, n):
		for j in range(0, c):
			if y_pred[i,j] == 1.0:
				# print(l_dec)
				# print(l_clx)
				l_dec += torch.log(torch.reshape(v[i, j], (1,)))
				l_clx += torch.log(res[i, j])
	l_dec = l_dec * (-1.0 / n)
	l_clx = l_clx * (-1.0 / n)
	return l_dec + l_clx

def train_test(model, img, label):
	optimizer = torch.optim.SGD(model.parameters(), lr=0.001, momentum=0.5)
	optimizer.zero_grad()
	# preprocess
	img.convert('RGB')

	scale = torchvision.transforms.Resize((224, 224))
	to_tensor = torchvision.transforms.ToTensor()
	norm = torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])

	img = scale(img)
	img = to_tensor(img)
	img = norm(img)
	img = img.unsqueeze(0)
	label = torch.from_numpy(label)

	img = torch.autograd.Variable(img)
	label = torch.autograd.Variable(label)
	output = model(img)
	out_label, out_vector = output
	# print("output: ")
	# print(out_label)
	loss = custom_loss2(label, out_label, out_vector)
	loss.backward()
	optimizer.step()
	# print("loss")
	# print(loss)

def train_test_image(model, img_location, label):
	im = PIL.Image.open(img_location)
	train_test(model, im, np.array(label))

# dataset

class EmotionDataset(torch.utils.data.dataset.Dataset):

	def __init__(self, dataset_list, dataset_folder, transform = None):
		self.dataset_list = dataset_list
		self.dataset_folder = dataset_folder
		self.transform = transform

	def __getitem__(self, index):
		filename = self.dataset_folder + self.dataset_list[index][1]
		# print("loading: "+filename)
		img = PIL.Image.open(filename)
		img.convert('RGB')
		if self.transform is not None:
			img = self.transform(img)

		label = self.dataset_list[index][2]
		label = np.array(label, np.float32)
		label = label[:8]
		label = torch.from_numpy(label)

		return img, label

	def __len__(self):
		return len(self.dataset_list)

data_transform = torchvision.transforms.Compose([torchvision.transforms.RandomSizedCrop(224), 
	torchvision.transforms.RandomHorizontalFlip(), 
	torchvision.transforms.Resize((224, 224)),
	torchvision.transforms.ToTensor(), 
	torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406], 
		std=[0.229, 0.224, 0.225])])

def separate_train_test(datalist, train_ratio, test_ratio):
	datalist_s = len(datalist)
	train_ratio = float(train_ratio)
	test_ratio = float(test_ratio)
	test_ratio2 = test_ratio / (train_ratio + test_ratio)
	test_size = test_ratio2 * datalist_s
	test_size = int(test_size)
	testlist = []
	for i in range(test_size):
		random_idx = random.randint(0, len(datalist)-1)
		# print("check")
		# print(len(datalist))
		# print(test_size)
		# print(random_idx)
		testlist.append(datalist.pop(random_idx))
	return datalist, testlist

# code

model = WSCNet(Bottleneck, [3, 4, 23, 3])

saved_weight = torch.load(PATH)

model.load_state_dict(saved_weight)

# train_test_image(model, "Flickr_LDL/images/1.jpg", [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0])

# train and test separation

f = open(dataset_datafile, "rb")
dataset_datapickle = pickle.load(f)

trainset, testset = separate_train_test(dataset_datapickle, 9, 1)

data_training = EmotionDataset(trainset, dataset_folder_loc, data_transform)

train_loader = torch.utils.data.DataLoader(data_training, batch_size = 10, shuffle = True)

lr_settings = []
for name, param in model.named_parameters():
	lr_set = {}
	if name=="fc":
		lr_set = {"params": param, "lr": 0.01}
	else:
		lr_set = {"params": param, "lr": 0.001}
	lr_settings.append(lr_set)

epoch = 0
optimizer = torch.optim.SGD(lr_settings, momentum = 0.9, weight_decay = 0.0005)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.1)

# loss test

# lossf = torch.nn.BCELoss()

def train(epoch):
	model.train()
	scheduler.step()
	for batch_idx, (data, target) in enumerate(train_loader):
		data, target = torch.autograd.Variable(data), torch.autograd.Variable(target)
		optimizer.zero_grad()
		output = model(data)
		out_label, out_vector = output
		# print("out?")
		# print(out_label)
		# print(target)
		loss = custom_loss(target, out_label, out_vector)
		# loss = lossf(out_label, target)
		loss.backward()
		optimizer.step()
		print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
			epoch, batch_idx * len(data), len(train_loader.dataset),
			100. * batch_idx / len(train_loader), loss.data[0]))
		checkpoint = { "epoch": epoch, "model": model, "scheduler": scheduler, "optimizer": optimizer.state_dict(), "state_dict": model.state_dict()}
		torch.save(checkpoint, train_checkpoint_filename)

for epoch in range(1, 3):
	train(epoch)