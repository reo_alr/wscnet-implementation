# WSCNet Implementation

Implementation of paper "Weakly supervised coupled networks for visual sentiment analysis" (http://openaccess.thecvf.com/content_cvpr_2018/html/Yang_Weakly_Supervised_Coupled_CVPR_2018_paper.html)

Dependencies:
- PyTorch
- Torchsummary (used for testing, optional)

Yang, J., She, D., Lai, Y. K., Rosin, P. L., & Yang, M. H. (2018). Weakly supervised coupled networks for visual sentiment analysis. In Proceedings of the IEEE conference on computer vision and pattern recognition (pp. 7584-7592).